--DROP TABLE IF EXISTS shop;



-- ************************************** shop

CREATE TABLE IF NOT EXISTS Hats_Gears
(
 id            integer NOT NULL AUTO_INCREMENT ,
 name          varchar(45) NOT NULL ,
 description   varchar(45) NOT NULL ,
 image         varchar(45) NOT NULL ,
 isCollectible tinyint NOT NULL ,
 stock         integer NOT NULL ,
 offSale       tinyint NOT NULL ,
 type          integer NOT NULL ,
 seller        integer NOT NULL COMMENT 'Seller' ,
 price         integer NOT NULL ,

PRIMARY KEY (id),
KEY fkIdx_35 (seller),
CONSTRAINT FK_34 FOREIGN KEY fkIdx_35 (seller) REFERENCES Users (id)
);





