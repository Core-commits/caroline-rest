-- DROP TABLE IF EXISTS Users;



-- ************************************** Users

CREATE TABLE IF NOT EXISTS Users
(
 id       integer NOT NULL AUTO_INCREMENT ,
 username varchar(45) NOT NULL ,
 password varchar(45) NOT NULL ,
 ip       varchar(45) NOT NULL ,
 currency integer NOT NULL ,
 isBanned tinyint NOT NULL ,
 isAdmin  tinyint NOT NULL ,
 status   varchar(45) NOT NULL ,
 isOnline tinyint NOT NULL ,
 bio      varchar(45) NOT NULL ,
 render   varchar(45) NOT NULL ,

PRIMARY KEY (id)
);





