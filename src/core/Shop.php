<?php
require_once 'Caroline.php';

class Shop extends Caroline {
    public $id;
    function __construct($id)
    {
        $this->id = $id;
    }

    public function newItem($name='Default', $desc = 'Default', $price = 0, $isLimited = false, $stock =0){
        $this->query('INSERT INTO Shop (name, description, image, stock, offsale, type, seller, price) VALUES (?, ?, ?, ?, ?, ?, ?, ?)');
    }
}