<?php
/**
 * @author Shigero 
 * @copyright Vistora (Some of the code here is originally Vistora's class.)
 * 
 * Hello all! Welcome to Caroline's deepest part of the heart. Have fun.
 * Class and Function List:
 * Function list:
 * - loadConfig()
 * - __construct()
 * - fetch()
 * Classes list:
 * - Caroline
 */
    try {
        $config = parse_ini_file("../config/config.ini", true);
    }
    catch(Exception $error) {
        die('An error occured loading the configuration file, please check the config.&nbsp;<code>' . $error . '</code>');
    }

try {
    $pdo = new PDO('mysql:host='.$config['Database']['host'].';dbname='.$config['Database']['dbname'].';charset=utf8', $config['Database']['username'], $config['Database']['password'], [\PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION, \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC, \PDO::ATTR_EMULATE_PREPARES => false, ]);
}
catch(Exception $e) {
    die('Wuh oh. We are undergoing database maintenance<br>For Developers: ' . $e);
}
class Caroline {
    function __construct(\PDO $pdo) {
        $this->conn = $pdo;
    }
	public function isAdmin($username)
	{
		if ($this->fetch("SELECT * FROM users WHERE username = ?", [$username])['is_admin'] == 1) {
			return true;
		} else {
			return false;
		}
    }
    	public function rows($query, $array = false)
	{
		try {
			$stmt = $this->conn->prepare($query);
			if ($array) {
			    $stmt->execute($array);
			} else {
			    $stmt->execute();
			}
			return $stmt->rowCount();
		} catch (Exception $e) {
			die('ERROR rows(): ' . $e);
		}
	}

	public function query($query, $array = false)
	{
		try {
			$stmt = $this->conn->prepare($query);
			if ($array) {
				$stmt->execute($array);
			} else {
				$stmt->execute();
			}
			if (explode(' ', $query)[0] == 'SELECT') {
				return $stmt;
			}
		} catch (Exception $e) {
			die('ERROR query(): ' . $e);
		}
	}

    public function fetch($query, $array      = false) {
        try {
            $stmt       = $this
                ->conn
                ->prepare($query);
            if (empty($array) || !$array) {
                $stmt->execute();
            }
            else {
                $stmt->execute($array);
            }
            return $stmt->fetch();
        }
        catch(Exception $e) {
            die('ERROR fetch(): ' . $e);
        }
    }
    	public function exists($query, $array)
	{
		try {
			$stmt = $this->conn->prepare($query);
			$stmt->execute($array);
			if ($stmt->rowCount >= 1) {
				return true;
			} else {
				return false;
			}
		} catch (Exception $e) {
			die('ERROR query(): ' . $e);
		}
	}

}
?>
